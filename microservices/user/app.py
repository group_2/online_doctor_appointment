from flask import Flask, render_template, url_for, request, flash, session, redirect, jsonify
from models.DbModel import *
from models.UserModel import *
from flask_cors import CORS
import requests

app = Flask(__name__)
app.run(debug=True)
app.secret_key = "2445hgh345"
app.config["SESSION_TYPE"] = "filesystem"

db_file = "../database.db"

@app.route("/register", methods=["POST"])
def register():
    # if 'user' in session and session['user']['logged_in']:
    #     return redirect("/")
    
    if request.method == "POST":
        error_html = ""
        fields = ['FirstName', 'LastName', 'Email', 'Password']
        fields_adv = {
            'FirstName': {
                'mandatory': True,
                'length': 1,
                'message': "Please enter first name<br>"
            }, 
            'LastName': {
                'mandatory': True,
                'length': 1,
                'message': "Please enter last name<br>"
            }, 
            'Email': {
                'mandatory': True,
                'length': 6,
                'message': "Please enter valid email<br>"
            }, 
            'Password': {
                'mandatory': True,
                'length': 6,
                'message': "Password must be at least 6 charcaters<br>"
            },
        }

        for key, value in fields_adv.items():
            if value['mandatory']:
                if len(request.form.get(key)) < value['length']:
                     error_html += value['message']

        if error_html != "":
            return f"<div class='alert alert-danger'>{error_html}</div>"

        formData = {}
        for field in fields:
            formData[field] = request.form.get(field)
        
        db = Database(db_file)
        conn = db.connection

        userObject = User(conn)

        if userObject.checkDuplicateUser(formData):
            message = "<div class='alert alert-warning'>Email already registered</div>"
            type = "duplicate"
        elif userObject.register_user(formData):
            message = "<div class='alert alert-success'>Registration Successful. You can login now.</div>"
            type = "success"
        else:
            message = "<div class='alert alert-danger'>Something Went Wrong!</div>"
            type = "error"

    return jsonify({"message": message, "type": type})
    #return render_template("register.html")

@app.route("/login", methods=['POST'])
def login():
    
    if request.method == "POST":
        print("HERE")
        db = Database(db_file)
        conn = db.connection

        fields = ['Email', 'Password']
        fields_adv = {
            'Email': {
                'mandatory': True,
                'length': 6,
                'message': "Please enter valid email<br>"
            }, 
            'Password': {
                'mandatory': True,
                'length': 6,
                'message': "Password must be at least 6 charcaters<br>"
            },
        }
        formData = {}
        data = request.get_json()
        print("FormData",data)
        for field in fields:
            formData[field] = data.get(field)

        userObject = User(conn)
        error_html = "" # empty string

        for key, value in fields_adv.items():
            if value['mandatory']:
                if len(data.get(key)) < value['length']:
                     error_html += value['message']

        if error_html == "":
            logged_user = userObject.doLogin(formData)
            
            user = {}
            
            if logged_user:
                user['logged_in'] = True
                user['user_info'] = [logged_user[0], logged_user[2]]
                return jsonify({"message": "success", "response": user})
            else:
                return jsonify({"message": "<div class='alert alert-danger'>Invalid login details!</div>", "response": user})
        else:
            return jsonify({"message": f"<div class='alert alert-warning'>{error_html}</div>", "response": user})
            
        
    #return render_template("login.html")

# USer profile information using GET request
@app.route("/profile", methods=['GET'])
def profile():
    
    dbObject = Database(db_file)

    conn = dbObject.connection

    userObject = User(conn)

    data = request.get_json()

    userDetails = userObject.getUserDetails(data.get('userid'))
    
    return jsonify({"message": "success", "response":userDetails})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)

CORS(app, supports_credentials=True)