import sqlite3

class Database:
    def __init__(self, path):
        self.connection = sqlite3.connect(path, check_same_thread = False)