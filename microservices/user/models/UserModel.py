from werkzeug.security import generate_password_hash, check_password_hash
class User:
  def __init__(self, conn) -> None:
    self.conn = conn

  def register_user(self,formData):
    cursor = self.conn.cursor()
    cols = ', '.join(list(formData.keys()))

    password_hash = generate_password_hash(formData['Password'])
    print(password_hash)
    query = f"insert into Users ({cols}) VALUES (\"{formData['FirstName']}\", \"{formData['LastName']}\", \"{formData['Email']}\", \"{password_hash}\")"

    try:
      cursor.execute(query)
      self.conn.commit()
      return True
    
    finally:
      self.conn.close()
  
  def checkDuplicateUser(self, formData):
    cursor = self.conn.cursor()
    query = f"SELECT count(*) as count FROM Users WHERE Email = \"{formData['Email']}\""

    try:
      cursor.execute(query)
      records = cursor.fetchall()
      count = records[0][0]
      if count > 0:
        return True
      else:
        return False
    except:
      return "Duplicate. Something Went Wrong!"
  
  def doLogin(self, formData):
    cursor = self.conn.cursor()
    
    query = "SELECT * FROM Users WHERE Email = \"{}\"".format(formData['Email'])
    
    try:
      cursor.execute(query)
      records = cursor.fetchall()
      if records and len(records[0][1]) > 0 and check_password_hash(records[0][4], formData['Password']):
        return records[0]
      else:
        return False
    except:
      return "DB. Something Went Wrong!"
  
  def getUserDetails(self, userid):
    cursor = self.conn.cursor()
    query = "SELECT * FROM Users WHERE UserId = \"{}\"".format(userid)
    
    try:
      cursor.execute(query)
      records = cursor.fetchall()
      
      if records and len(records[0][1]) > 0:
        return records[0]
      else:
        return False
    except:
      return "Something Went Wrong!"