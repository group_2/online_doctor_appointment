from flask import Flask, render_template, url_for, request, flash, session, redirect, jsonify
from models.DbModel import *
from models.BookingModel import *
from flask_cors import CORS
import datetime, socket
import json

app = Flask(__name__)
app.run(debug=True)
app.secret_key = "2445hgh345"
app.config["SESSION_TYPE"] = "filesystem"

db_file = "../database.db"

# booking microservice
@app.route("/add_booking", methods=['POST'])
def add_booking():
    
    dbObject = Database(db_file)

    conn = dbObject.connection

    bookingObject = Booking(conn)

    if request.method == "POST":
        error_html = ""
        fields = ['UserId', 'DoctorId', 'BookingDate', 'StartTime', 'EndTime']
        fields_adv = {
            'DoctorId': {
                'mandatory': True,
                'message': "Please select a Doctor<br>"
            }, 
            'BookingDate': {
                'mandatory': True,
                'message': "Please select a Date<br>"
            }, 
            'StartTime': {
                'mandatory': True,
                'message': "Please select Start Time<br>"
            }, 
            'EndTime': {
                'mandatory': True,
                'message': "Please select End Time<br>"
            }, 
        }
        data = request.get_json()
        print("Data: ",data)
        for key, value in fields_adv.items():
            if value['mandatory']:
                if len(data.get(key)) < 1:
                     error_html += value['message']

        if error_html != "":
            return jsonify({"type":"add_error","message":f"<div class='alert alert-danger'>{error_html}</div>"})

        formData = {}
        for field in fields:
            formData[field] = data.get(field)

        if bookingObject.addBooking(formData):
            return jsonify({"type":"added","message":"<div class='alert alert-success'>Booking Successful</div>"})

@app.route("/get_all_doctors", methods=["GET"])
def get_all_doctors():
    
    dbObject = Database(db_file)

    conn = dbObject.connection

    bookingObject = Booking(conn)

    doctors = bookingObject.getAllDoctors()
    
    return jsonify({"message": "success", "response": doctors})

@app.route("/get_user_bookings", methods=["GET"])
def get_user_bookings():
    
    dbObject = Database(db_file)

    conn = dbObject.connection

    bookingObject = Booking(conn)
    data = request.get_json()

    bookings = bookingObject.getUserBookings(data.get('userid'))
    
    return jsonify({"message": "success", "response": bookings})

@app.route("/get_timeslots", methods=['POST'])
def get_timeslots():
    dbObject = Database(db_file)

    conn = dbObject.connection

    bookingObject = Booking(conn)
    data = request.get_json()
    print("DoctorId",data.get('DoctorId'))
    doctor_info = bookingObject.getTimeSlots(data.get('DoctorId'))
    if doctor_info:
        start_time = doctor_info[0]
        end_time = doctor_info[1]
        time_slots = bookingObject.generateTimeSlots(start_time, end_time)
        time_json = json.dumps(time_slots)
            
        return time_json
    else:
        return jsonify({"error": "Doctor not found"})


@app.route("/cancel", methods=['POST'])
def cancel():
    if id:
      dbObject = Database(db_file)
      conn = dbObject.connection
      bookingObject = Booking(conn)

      data = request.get_json()
      bookingObject.deleteBooking(data.get('id'), data.get('status'))
      return jsonify({"message": "success"})
    
    return jsonify({"message": "Invalid Request"})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)

CORS(app, supports_credentials=True)