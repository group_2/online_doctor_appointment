class Booking:
  def __init__(self, conn) -> None:
    self.conn = conn

  def getAllDoctors(self):
    cursor = self.conn.cursor()
    query = "SELECT * FROM Doctors ORDER BY LastName"

    try:
      cursor.execute(query)
      records = cursor.fetchall()
      
      if records:
        return records
      else:
        return False
    except:
      return "Something Went Wrong!"

  def getTimeSlots(self, doctor_id):
    cursor = self.conn.cursor()
    query = f"SELECT Open, Close FROM Doctors WHERE DoctorId = {doctor_id}"

    try:
      cursor.execute(query)
      doctor_info = cursor.fetchone()
      if doctor_info:
        return doctor_info
      else:
          return False
    except:
      return "Something Went Wrong!"
    
  def generateTimeSlots(self,start_time, end_time):
    time_slots = []
    current_time = start_time
    while current_time < end_time:
        time_slots.append(current_time)
        # Increment time by half an hour
        current_time = self.increment_time(current_time)
    return time_slots

  # function to increment time by half an hour
  def increment_time(self,time_str):
    hour, minute = map(int, time_str.split(':'))
    minute += 30
    if minute >= 60:
        hour += 1
        minute -= 60
    return '{:02}:{:02}'.format(hour, minute)
  
  def addBooking(self,formData):
    cursor = self.conn.cursor()
    cols = ', '.join(list(formData.keys()))
    
    query = f"insert into Booking ({cols}) VALUES (\"{formData['UserId']}\", \"{formData['DoctorId']}\", \"{formData['BookingDate']}\", \"{formData['StartTime']}\", \"{formData['EndTime']}\")"

    try:
      cursor.execute(query)
      self.conn.commit()
      return True
    
    finally:
      self.conn.close()

  def getUserBookings(self, user):
    cursor = self.conn.cursor()
    query = f"""SELECT Booking.*, Doctors.FirstName || ' ' || Doctors.LastName as Doctor 
            FROM Booking 
            JOIN Doctors on Booking.DoctorId=Doctors.DoctorId 
            WHERE Booking.UserId={user} ORDER BY Booking.BookingDate ASC, StartTime ASC
                """

    try:
      cursor.execute(query)
      records = cursor.fetchall()
      
      if records:
        return records
      else:
        return False
    except:
      return "Something Went Wrong!"
    
  def deleteBooking(self, id, status):
    cursor = self.conn.cursor()
    
    query = f"UPDATE Booking SET Status='{status}' WHERE BookingId={id}"

    try:
      cursor.execute(query)
      self.conn.commit()
      return True
    
    finally:
      self.conn.close()

  def getSingleBooking(self, id):
    cursor = self.conn.cursor()
    query = f"SELECT * FROM Booking WHERE BookingId = {id}"

    try:
      cursor.execute(query)
      records = cursor.fetchall()
      if records:
        return records[0]
      else:
        return False
    except:
      return "Duplicate. Something Went Wrong!"
    
  def updateBooking(self, formData):
    cursor = self.conn.cursor()
    
    query = f"""UPDATE Booking SET 
    DoctorId={formData['DoctorId']},
    UserId={formData['UserId']},
    BookingDate='{formData['BookingDate']}',
    StartTime='{formData['StartTime']}',
    EndTime='{formData['EndTime']}',
    Status='{formData['Status']}'
    WHERE BookingId={formData['BookingId']}
    """

    try:
      cursor.execute(query)
      self.conn.commit()
      return True
    
    finally:
      self.conn.close()