from flask import Flask, render_template, url_for, request, flash, session, redirect, jsonify
import json
import requests
#from flask_cors import CORS

app = Flask(__name__)
app.run(debug=True)
app.secret_key = "2445hgh345"
app.config["SESSION_TYPE"] = "filesystem"

db_file = "../database.db"

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/register", methods=["GET"])
def register():
    return render_template("register.html")

@app.route("/login", methods=['GET', 'POST'])
def login():
    if 'user' in session and session['user']['logged_in']:
        return redirect("/")
    
    if request.method == "POST":
        print(request.form.get("Email"), request.form.get("Password"))
        formData = request.form.to_dict(flat=True)
        response = requests.post("http://127.0.0.1:5001/login", json=formData)
        
        res_json = response.json()
        print(res_json)
        if res_json['message'] == "success":
            session['user'] = {}
            session['user']['logged_in'] = True
            session['user']['user_info'] = [res_json['response']['user_info'][0], res_json['response']['user_info'][1]]
            return "success"
        else:
            return res_json['message']
    return render_template("login.html")

@app.route("/logout")
def logout():
    del session['user']
    return redirect("/")

@app.route("/profile")
def profile():
    if 'user' not in session:
        return redirect("/login")
    
    # using GET Request
    url = "http://127.0.0.1:5001/profile"
    userDetails_response = requests.get(url, json={'userid': session['user']['user_info'][0]})

    if userDetails_response.status_code == 200:
        userDetails = userDetails_response.json()['response']
    
    return render_template("profile.html", user = userDetails)


@app.route("/booking", methods=['GET','POST'])
def booking():

    if request.method == "POST":
        #print("POSTing booking")
        formData = request.form.to_dict(flat=True)
        response = requests.post("http://127.0.0.1:5003/add_booking", json=formData)
        
        res_json = response.json()
        #print(res_json)
        if response.status_code == 200:
            return res_json

    bookings = []
    if 'user' in session and session['user']['logged_in']:
        bookings_response = requests.get("http://127.0.0.1:5003/get_user_bookings", json={'userid': session['user']['user_info'][0]})
        #print("bookings", bookings_response.json()['response'])
        bookings = bookings_response.json()['response']

    doctors_response = requests.get("http://127.0.0.1:5003/get_all_doctors")
    #print("get_all_doctors, doctors_response", doctors_response)
    doctors = doctors_response.json()['response']

    status = {
        "Booked": {
            "class": "badge text-bg-success", 
            "warning": "Are you sure you want to Cancel this booking?",
            "status": "Cancelled",
            "button": "Cancel",
            }, 
        "Cancelled": {
            "class": "badge text-bg-danger", 
            "warning": "Are you sure you want to Book this again?",
            "status": "Booked",
            "button": "Rebook",
            }
        }

    return render_template("booking.html", doctors = doctors, bookings = bookings, status = status)

@app.route("/cancel/<id>/<status>")
def cancel_bookings(id, status):
    if id:
        cancel_response = requests.post("http://127.0.0.1:5003/cancel", json={'id': id, 'status': status})
        if cancel_response.status_code == 200:
            return redirect("/booking")
    
    return "Invalid Request"

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)

'''
@app.route("/change_booking/<id>", methods=['POST', 'GET'])
def change_booking(id):
    if id:
        dbObject = Database(db_file)
        conn = dbObject.connection
        bookingObject = Booking(conn)

        doctors = bookingObject.getAllDoctors()
        edit_bookings = bookingObject.getSingleBooking(id)
        print(edit_bookings)

        if request.method == "POST":
            fields = ['UserId', 'DoctorId', 'BookingDate', 'StartTime', 'EndTime', 'BookingId','Status']
            formData = {}
            for field in fields:
                formData[field] = request.form.get(field)
            bookingObject.updateBooking(formData)
            #return redirect("/booking")
            return "changed"

        return render_template("change_booking.html", booking = edit_bookings, doctors = doctors)

    return "Invalid Request"


#

'''