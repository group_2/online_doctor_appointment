from flask import Flask, render_template, url_for, request, flash, session, redirect, jsonify
from models.DbModel import *
from models.UserModel import *
from models.BookingModel import *
import datetime, socket
import json

app = Flask(__name__)
app.run(debug=True)
app.secret_key = "2445hgh345"
app.config["SESSION_TYPE"] = "filesystem"

db_file = "database.db"


# route -> https://127.0.0.1/user/create, https://127.0.0.1/user/edit

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/register", methods=["GET", "POST"])
def register():
    if 'user' in session and session['user']['logged_in']:
        return redirect("/")
    
    if request.method == "POST":
        error_html = ""
        fields = ['FirstName', 'LastName', 'Email', 'Password']
        fields_adv = {
            'FirstName': {
                'mandatory': True,
                'length': 1,
                'message': "Please enter first name<br>"
            }, 
            'LastName': {
                'mandatory': True,
                'length': 1,
                'message': "Please enter last name<br>"
            }, 
            'Email': {
                'mandatory': True,
                'length': 6,
                'message': "Please enter valid email<br>"
            }, 
            'Password': {
                'mandatory': True,
                'length': 6,
                'message': "Password must be at least 6 charcaters<br>"
            },
        }

        for key, value in fields_adv.items():
            if value['mandatory']:
                if len(request.form.get(key)) < value['length']:
                     error_html += value['message']

        if error_html != "":
            return f"<div class='alert alert-danger'>{error_html}</div>"

        formData = {}
        for field in fields:
            formData[field] = request.form.get(field)
        
        db = Database(db_file)
        conn = db.connection

        userObject = User(conn)

        if userObject.checkDuplicateUser(formData):
            return "<div class='alert alert-warning'>Email already registered</div>"
        
        if userObject.register_user(formData):
            return "<div class='alert alert-success'>Registration Successful. You can login now.</div>"
    
    return render_template("register.html")

@app.route("/login", methods=['GET', 'POST'])
def login():
    if 'user' in session and session['user']['logged_in']:
        return redirect("/")
    
    if request.method == "POST":

        db = Database(db_file)
        conn = db.connection

        fields = ['Email', 'Password']
        fields_adv = {
            'Email': {
                'mandatory': True,
                'length': 6,
                'message': "Please enter valid email<br>"
            }, 
            'Password': {
                'mandatory': True,
                'length': 6,
                'message': "Password must be at least 6 charcaters<br>"
            },
        }
        formData = {}
        for field in fields:
            formData[field] = request.form.get(field)

        userObject = User(conn)
        error_html = "" # empty string

        for key, value in fields_adv.items():
            if value['mandatory']:
                if len(request.form.get(key)) < value['length']:
                     error_html += value['message']

        if error_html == "":
            logged_user = userObject.doLogin(formData)
            
            if logged_user:
                session['user'] = {}
                session['user']['logged_in'] = True
                session['user']['user_info'] = [logged_user[0], logged_user[2]]
                return "success"
            else:
                return "<div class='alert alert-danger'>Invalid login details!</div>"
        else:
            return f"<div class='alert alert-warning'>{error_html}</div>"
        
    return render_template("login.html")

@app.route("/logout")
def logout():
    del session['user']
    return redirect("/")

@app.route("/profile")
def profile():
    if 'user' not in session:
        return redirect("/login")
    
    dbObject = Database(db_file)

    conn = dbObject.connection

    userObject = User(conn)
    
    userDetails = userObject.getUserDetails(session['user']['user_info'][0])
    
    return render_template("profile.html", user = userDetails)

# booking microservice
@app.route("/booking", methods=['GET','POST'])
def booking():
    
    dbObject = Database(db_file)

    conn = dbObject.connection

    bookingObject = Booking(conn)

    if request.method == "POST":
        error_html = ""
        fields = ['UserId', 'DoctorId', 'BookingDate', 'StartTime', 'EndTime']
        fields_adv = {
            'DoctorId': {
                'mandatory': True,
                'message': "Please select a Doctor<br>"
            }, 
            'BookingDate': {
                'mandatory': True,
                'message': "Please select a Date<br>"
            }, 
            'StartTime': {
                'mandatory': True,
                'message': "Please select Start Time<br>"
            }, 
            'EndTime': {
                'mandatory': True,
                'message': "Please select End Time<br>"
            }, 
        }

        for key, value in fields_adv.items():
            if value['mandatory']:
                if len(request.form.get(key)) < 1:
                     error_html += value['message']

        if error_html != "":
            return f"<div class='alert alert-danger'>{error_html}</div>"

        formData = {}
        for field in fields:
            formData[field] = request.form.get(field)

        if bookingObject.addBooking(formData):
            return "<div class='alert alert-success'>Booking Successful</div>"
        
    doctors = bookingObject.getAllDoctors()
    bookings = []
    if 'user' in session and session['user']['logged_in']:
        bookings = bookingObject.getUserBookings(session['user']['user_info'][0])

    status = {
        "Booked": {
            "class": "badge text-bg-success", 
            "warning": "Are you sure you want to Cancel this booking?",
            "status": "Cancelled",
            "button": "Cancel",
            }, 
        "Cancelled": {
            "class": "badge text-bg-danger", 
            "warning": "Are you sure you want to Book this again?",
            "status": "Booked",
            "button": "Rebook",
            }
        }

    return render_template("booking.html", doctors = doctors, bookings = bookings, status = status)

@app.route("/get_timeslots", methods=['POST'])
def get_timeslots():
    if request.method == 'POST':
        doctor_id = request.form.get('doctorId')

        dbObject = Database(db_file)

        conn = dbObject.connection

        bookingObject = Booking(conn)

        doctor_info = bookingObject.getTimeSlots(doctor_id)
        if doctor_info:
            start_time = doctor_info[0]
            end_time = doctor_info[1]
            time_slots = bookingObject.generateTimeSlots(start_time, end_time)
            time_json = json.dumps(time_slots)
            
            return time_json
        else:
            return jsonify({"error": "Doctor not found"})

@app.route("/cancel/<id>/<status>")
def cancel_bookings(id, status):
    if id:
        dbObject = Database(db_file)
        conn = dbObject.connection
        bookingObject = Booking(conn)

        bookingObject.deleteBooking(id, status)

        return redirect("/booking")
    
    return "Invalid Request"

@app.route("/change_booking/<id>", methods=['POST', 'GET'])
def change_booking(id):
    if id:
        dbObject = Database(db_file)
        conn = dbObject.connection
        bookingObject = Booking(conn)

        doctors = bookingObject.getAllDoctors()
        edit_bookings = bookingObject.getSingleBooking(id)
        print(edit_bookings)

        if request.method == "POST":
            fields = ['UserId', 'DoctorId', 'BookingDate', 'StartTime', 'EndTime', 'BookingId','Status']
            formData = {}
            for field in fields:
                formData[field] = request.form.get(field)
            bookingObject.updateBooking(formData)
            #return redirect("/booking")
            return "changed"

        return render_template("change_booking.html", booking = edit_bookings, doctors = doctors)

    return "Invalid Request"