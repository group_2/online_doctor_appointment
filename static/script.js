
$(document).ready(function(){

    $('#doctorSelect').trigger('change');

    $("#form-submit").on("click", function(e){
        $('.alert').hide('slow');
        e.preventDefault();
        var form = $("#form");
        $.ajax({
            url : form.attr("action"),
            data : form.serialize(),
            method : form.attr("method"),
            success : function (response) {
                if (response == "changed"){
                    form.before("<div class='alert alert-success'>Booking Changed Successfully</div>");
                    setTimeout(function () {
                        $('.alert').hide('slow');
                        window.location.href = "/booking";
                    }, 2000);
                }
                else{
                    form.before(response);
                    setTimeout(function () {
                        $('.alert').hide('slow');
                        window.location.href = window.location.href;
                    }, 3000);
                }
            }
        });
    });

    $("#login_btn").on("click", function (event) {
        console.log("click login");
        event.preventDefault();
        $(".alert").hide("slow");
        var form = $("#login_form");

        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            method: form.attr("method"),
            success: function (response) {
                if (response == "success") {
                    form.before("<div class='alert alert-success'>Logged in!</div>")
                    setTimeout(function () {
                        window.location = "./";
                    }, 2000);
                }
                else{
                    form.before(response);
                    setTimeout(function () {
                        $(".alert").hide("slow");
                    }, 2000);
                }
            }
        });
    });

    $('#doctorSelect').change(function(){
        console.log("change");
        var doctorId = $(this).val();
        
        $.ajax({
        url: '/get_timeslots',
        type: 'POST',
        data: { doctorId: doctorId },
        success: function(response){
            var timeSlots = JSON.parse(response);
            
            $('#startTimeSelect').empty();
            $('#endTimeSelect').empty();
            
            $.each(timeSlots, function(index, time){
            $('#startTimeSelect').append('<option value="' + time + '">' + time + '</option>');
            $('#endTimeSelect').append('<option value="' + time + '">' + time + '</option>');
            });
            },
        });
    });

});